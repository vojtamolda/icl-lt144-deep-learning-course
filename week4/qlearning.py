import gym
import math
import pickle
import random
import argparse
import statistics
import collections


class QLearning:

    class Table(dict):
        def __init__(self, state_space: gym.spaces.Discrete, action_space: gym.spaces.Discrete):
            assert type(action_space) == gym.spaces.Discrete
            super().__init__()
            self.state_space = state_space
            self.action_space = action_space

            for state in range(self.state_space.n):
                for action in range(self.action_space.n):
                    self[state, action] = 0.0

        def best_action(self, state: int) -> int:
            best_q, best_action = -math.inf, None

            for action in random.sample(range(self.action_space.n), self.action_space.n):
                if self[state, action] > best_q:
                    best_q, best_action = self[state, action], action

            return best_action

    class Trajectory:
        Transition = collections.namedtuple('Transition', ('state', 'action', 'reward', 'next_state', 'next_action'))

        def __init__(self, states: list, actions: list, rewards: list):
            assert len(states) == len(actions) + 1 == len(rewards) + 1
            self.states = states
            self.actions = actions
            self.rewards = rewards

        def __len__(self):
            return len(self.rewards)

        def __iter__(self):
            self.iter = iter(range(len(self)))
            return self

        def __reversed__(self):
            self.iter = iter(reversed(range(len(self))))
            return self

        def __next__(self):
            idx = next(self.iter)
            if not 0 <= idx < len(self):
                raise StopIteration

            action, state, reward = self.actions[idx], self.states[idx], self.rewards[idx]
            next_state = self.states[idx + 1]
            next_action = self.actions[idx + 1] if idx + 1 < len(self) else None

            transition = QLearning.Trajectory.Transition(state, action, reward, next_state, next_action)
            return transition

        def reward(self):
            return sum(self.rewards)

    def __init__(self, learning_rate: float = 0.5, exploration_rate: float = 1.0, num_episodes: int = 10_000):
        self.learning_rate = learning_rate
        self.exploration_rate = exploration_rate
        self.num_episodes = num_episodes

    def train(self, env: gym.Env) -> 'QLearning.Table':
        q = QLearning.Table(env.observation_space, env.action_space)

        for episode in range(self.num_episodes):
            exploration_rate = max((1 - 2 * episode / self.num_episodes) * self.exploration_rate, 0)
            trajectory = self.episode_trajectory(q, env, exploration_rate)
            self.improve_table(q, trajectory)

        return q

    @staticmethod
    def episode_trajectory(q: 'QLearning.Table', env: gym.Env, exploration_rate: float = 0.0,
                           render: bool = False) -> 'QLearning.Trajectory':
        state = env.reset()
        states, actions, rewards = [state], [], []

        done = False
        while not done:
            action = q.best_action(state) if random.random() > exploration_rate else env.action_space.sample()

            state, reward, done, info = env.step(action)
            if render:
                env.render(mode='human')

            states += [state]
            actions += [action]
            rewards += [reward]

        return QLearning.Trajectory(states, actions, rewards)

    def improve_table(self, q: 'QLearning.Table', trajectory: 'QLearning.Trajectory'):
        for (state, action, reward, next_state, next_action) in reversed(trajectory):
            if next_action is None:
                q[state, action] = reward
            else:
                curr_q = q[state, action]
                best_next_action = q.best_action(next_state)
                best_next_q = q[next_state, best_next_action]
                q[state, action] += self.learning_rate * (reward + best_next_q - curr_q)


class SARSA(QLearning):

    def improve_table(self, q: 'QLearning.Table', trajectory: 'QLearning.Trajectory'):
        for (state, action, reward, next_state, next_action) in trajectory:
            if next_action is None:
                q[state, action] = reward
            else:
                curr_q = q[state, action]
                next_q = q[next_state, next_action]
                q[state, action] += self.learning_rate * (reward + next_q - curr_q)


if __name__ == '__main__':
    algorithms = {'q': QLearning, 'sarsa': SARSA}

    cli = argparse.ArgumentParser()
    cli.add_argument('mode', type=str, choices=('train', 'eval'), default='eval',
                     help='policy evaluation or training mode switch (default: eval)')
    cli.add_argument('--algorithm', type=str, choices=algorithms.keys(), default='q', metavar='ALGO',
                     help='learning algorithm (default: q)')
    cli.add_argument('--env_id', type=str, default='FrozenLake-v0', metavar='ENV',
                     help='gym environment name (default: FrozenLake-v0)')
    cli.add_argument('--file', type=str, default='table.pkl', metavar='FILE',
                     help='file where to load or save Q value table (default: table.pkl)')
    cli.add_argument('--num_episodes', type=int, default=10_000, metavar='N',
                     help='number of episode rollouts used for training or evaluation (default: 10_000)')
    cli.add_argument('--learning_rate', type=float, default=0.5, metavar='LR',
                     help='q table learning rate (default: 0.5)')
    cli.add_argument('--exploration_rate', type=float, default=1.0, metavar='EPS',
                     help='initial environment exploration rate (default: 1.0)')
    cli.add_argument('--render', action='store_true',
                     help='enable environment rending for humans')
    args = cli.parse_args()

    env = gym.make(args.env_id)

    if args.mode == 'eval':
        with open(args.file, 'rb') as file:
            q = pickle.load(file)

        trajectories = [QLearning.episode_trajectory(q, env, render=args.render) for _ in range(args.num_episodes)]
        rewards = [trajectory.reward() for trajectory in trajectories]
        print(f"Reward '{args.env_id}': {statistics.mean(rewards):.2f} ± {statistics.stdev(rewards):.2f}")

    if args.mode == 'train':
        algorithm = algorithms[args.algorithm](args.learning_rate, args.exploration_rate, args.num_episodes)
        q = algorithm.train(env)

        with open(args.file, 'wb') as file:
            pickle.dump(q, file)

        print(f"Done")

# Imperial College London Course LT144 - Deep Learning (Autumn 2018)


Lecture notes, assignments and other materials can be downloaded from the [course webpage](https://www.deeplearningmathematics.com) and homeworks on [GitHub](https://github.com/pukkapies/dl-imperial-maths).

## Assignments

 - [X] Week 1: Linear Regression [`readme.md`](week1/readme.md)
 - [X] Week 2: Feedforward Network [`readme.md`](week2/readme.md)
 - [X] Week 3: Convolutional Network [`readme.md`](week3/readme.md)
 - [X] Week 4: Q-Learning [`readme.md`](week4/readme.md)
 - [X] Week 5: Policy Gradient [`readme.md`](week5/readme.md)
 - [X] Week 6: Generative Models [`readme.md`](week6/readme.md)


## Course Description

Deep Learning is a fast-evolving field in artificial intelligence that has been driving breakthrough advances in many application areas in recent years. It has become one of the most in-demand skillsets in machine learning and AI, far exceeding the supply of people with an expertise in this field. This course is aimed at PhD students within the Mathematics department at Imperial College who have no prior knowledge or experience of the field. It will cover the foundations of Deep Learning, including the various types of neural networks used for supervised and unsupervised learning. Practical tutorials in TensorFlow/PyTorch are an integral part of the course, and will enable students to build and train their own deep neural networks for a range of applications. The course also aims to describe the current state-of-the-art in various areas of Deep Learning, theoretical underpinnings and outstanding problems.

Topics covered in this course will include: 

* Convolutional and recurrent neural networks
* Reinforcement Learning
* Generative Adversarial Networks (GANs)
* Variational autoencoders (VAEs)
* Theoretical foundations of Deep Learning


## Requirements

To complete the coursework and run the notebooks you will need to install Tensorflow and PyTorch (as well as other scientific packages, especially numpy). These can be installed using pip; alternatively Tensorflow/PyTorch can be installed using Anaconda (preferred for PyTorch). Jupyter is conveniently installed with Anaconda, or it can also be installed using pip. Relevant links are given below:

* Installing Anaconda: [https://www.anaconda.com/download/](https://www.anaconda.com/download/)
* Installing Jupyter: [https://jupyter.readthedocs.io/en/latest/install.html](https://jupyter.readthedocs.io/en/latest/install.html)
* Installing Tensorflow via Anaconda: [https://www.anaconda.com/blog/developer-blog/tensorflow-in-anaconda/](https://www.anaconda.com/blog/developer-blog/tensorflow-in-anaconda/)
* Installing Tensorflow using pip: [https://www.tensorflow.org/install/](https://www.tensorflow.org/install/)
* Installing PyTorch via Anaconda/pip: [https://pytorch.org/get-started/locally/](https://pytorch.org/get-started/locally/)
* Installing torchtext via pip (Anaconda install unavailable): [https://github.com/pytorch/text](https://github.com/pytorch/text)
* Installing OpenAI Gym using pip (Anaconda install unavailable): [https://gym.openai.com/docs/](https://gym.openai.com/docs/)

The PyTorch notebooks require Python 3. Different Python versions can be managed via Anaconda.

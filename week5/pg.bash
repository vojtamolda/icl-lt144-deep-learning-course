#!/usr/bin/env bash

# Show training progress in TensorBoard
tensorboard --logdir=log &

# Train Policy Gradient Agent
echo "Policy Gradient Algorithm:"
python3 pg.py train --algorithm=pg --env_id=CartPole-v0 --learning_rate=0.0005 --num_episodes=500 \
                    --file=pg.pkl --logdir=log

# Evaluate and Demo the Policy Gradient Agent
python3 pg.py eval --algorithm=pg --env_id=CartPole-v0 --num_episodes=100 --file=pg.pkl
python3 pg.py eval --algorithm=pg --env_id=CartPole-v0 --num_episodes=2 --file=pg.pkl --render

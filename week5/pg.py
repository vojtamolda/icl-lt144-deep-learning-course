import pickle
import petname
import argparse
import statistics

import gym
import torch
import torch.nn
import torch.nn.functional
import numpy as np
import tensorboardX


class PolicyGradient:

    class Policy(torch.nn.Sequential):
        def __init__(self, state_space: gym.spaces.Box, action_space: gym.spaces.Discrete):
            assert type(action_space) == gym.spaces.Discrete

            self.state_space = state_space
            self.action_space = action_space

            layers = [
                torch.nn.Linear(state_space.shape[0], 32),
                torch.nn.ReLU(),
                torch.nn.Linear(32, action_space.n)
            ]
            super().__init__(*layers)

        def best_action(self, state: np.ndarray) -> (int, torch.Tensor):
            state_batch = torch.from_numpy(np.expand_dims(state.astype(np.float32), axis=0))
            action_scores = self.forward(state_batch)

            action_distribution = torch.distributions.Categorical(logits=action_scores)
            action = action_scores.argmax()
            log_prob = action_distribution.log_prob(action)

            return int(action), log_prob[0]

        def sample_action(self, state: np.ndarray) -> (int, torch.Tensor):
            state_batch = torch.from_numpy(np.expand_dims(state.astype(np.float32), axis=0))
            action_scores = self.forward(state_batch)

            action_distribution = torch.distributions.Categorical(logits=action_scores)
            action = action_distribution.sample()
            log_prob = action_distribution.log_prob(action)

            return int(action), log_prob[0]

    class Trajectory:
        def __init__(self, states: list, actions: list, log_probs: list, rewards: list):
            self.states = torch.tensor(states, dtype=torch.float32)
            self.actions = torch.tensor(actions)
            self.log_probs = torch.stack(log_probs)
            self.rewards = torch.tensor(rewards, dtype=torch.float32)

        def __len__(self) -> int:
            return self.rewards.shape[0]

        def reward_sum(self) -> float:
            return float(self.rewards.sum())

        def reward_to_go(self) -> torch.Tensor:
            return self.rewards.cumsum(dim=0).flip(dims=[0])

    def __init__(self, learning_rate: float = 0.0005, num_episodes: int = 1_000, logdir: str = 'logdir'):
        self.writer = tensorboardX.SummaryWriter(logdir)
        self.learning_rate = learning_rate
        self.num_episodes = num_episodes

    def train(self, env: gym.Env) -> Policy:
        agent = self.Policy(env.observation_space, env.action_space)
        optimizer = torch.optim.Adam(agent.parameters(), lr=self.learning_rate)

        self.writer.add_text('params/algorithm', f'{type(self)}')
        self.writer.add_text('params/optimizer', f'{type(optimizer)}')
        self.writer.add_text('params/learning_rate', f'{self.learning_rate}')
        self.writer.add_text('params/num_episodes', f'{self.num_episodes}')
        self.writer.add_graph(agent, torch.zeros((1,) + env.observation_space.shape))

        agent.train()
        for episode in range(self.num_episodes):
            trajectory = self.episode_trajectory(agent, env, sample=True)
            loss, reward = self.improve_policy(optimizer, trajectory)

            self.writer.add_scalar('loss', loss, episode)
            self.writer.add_scalar('reward', reward, episode)
        agent.eval()

        return agent

    def episode_trajectory(self, agent: Policy, env: gym.Env, sample: bool = False, render: bool = False) -> Trajectory:
        state = env.reset()
        states, actions, log_probs, rewards = [state], [], [], []

        done = False
        while not done:
            action, log_prob = agent.sample_action(state) if sample else agent.best_action(state)
            state, reward, done, info = env.step(action)
            if render:
                env.render(mode='human')

            states += [state]
            actions += [action]
            log_probs += [log_prob]
            rewards += [reward]

        return self.Trajectory(states, actions, log_probs, rewards)

    def improve_policy(self, optimizer: torch.optim.Optimizer, trajectory: Trajectory) -> (float, float):
        reward_to_go = trajectory.reward_to_go()
        actions_log_prob = trajectory.log_probs

        loss = - (actions_log_prob * reward_to_go).mean()
        loss.backward()
        optimizer.step()

        return float(loss), trajectory.reward_sum()


if __name__ == '__main__':
    algorithms = {'pg': PolicyGradient, 'ac': None}

    cli = argparse.ArgumentParser()
    cli.add_argument('mode', type=str, choices=('train', 'eval'), default='eval',
                     help='policy evaluation or training mode switch (default: eval)')
    cli.add_argument('--algorithm', type=str, choices=algorithms.keys(), default='pg', metavar='ALGO',
                     help='learning algorithm (default: pg)')
    cli.add_argument('--env_id', type=str, default='CartPole-v0', metavar='ENV',
                     help='gym environment name (default: CartPole-v0)')
    cli.add_argument('--file', type=str, default='agent.pkl', metavar='FILE',
                     help='file where to load or save learned policy table (default: agent.pkl)')
    cli.add_argument('--num_episodes', type=int, default=1_000, metavar='N',
                     help='number of episode rollouts used for training or evaluation (default: 1_000)')
    cli.add_argument('--learning_rate', type=float, default=0.0005, metavar='LR',
                     help='q table learning rate (default: 0.0005)')
    cli.add_argument('--render', action='store_true',
                     help='enable environment rending for humans')
    cli.add_argument('--logdir', type=str, default=f'log/{petname.generate()}', metavar='DIR',
                     help='logging directory for TensorBoard (default: log/random-animal)')
    args = cli.parse_args()

    env = gym.make(args.env_id)
    algorithm = algorithms[args.algorithm](args.learning_rate, args.num_episodes, args.logdir)

    if args.mode == 'eval':
        with open(args.file, 'rb') as file:
            agent = pickle.load(file)

        trajectories = [algorithm.episode_trajectory(agent, env, render=args.render) for _ in range(args.num_episodes)]
        rewards = [trajectory.reward_sum() for trajectory in trajectories]
        print(f"Reward '{args.env_id}': {statistics.mean(rewards):.2f} ± {statistics.stdev(rewards):.2f}")

    if args.mode == 'train':
        agent = algorithm.train(env)

        with open(args.file, 'wb') as file:
            pickle.dump(agent, file)
        print(f"Done")

#! /usr/bin/env python3

import petname
import argparse

import ignite
import ignite.contrib.handlers
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data
import torchvision.datasets as datasets
import torchvision.transforms as transforms
import tensorboardX


class FullyConnectedNetwork(nn.Sequential):
    def __init__(self, in_size: int, hidden_sizes: list, out_size: int):
        layers, prev_size = [], in_size
        for hidden_size in hidden_sizes:
            layers += [nn.Linear(prev_size, hidden_size)]
            layers += [nn.ReLU(inplace=True)]
            layers += [nn.Dropout(p=0.2)]
            prev_size = hidden_size
        layers += [nn.Linear(prev_size, out_size)]
        super().__init__(*layers)

    def forward(self, input: torch.Tensor):
        flat = input.view([input.shape[0], -1])
        return super().forward(flat)


def train(args: argparse.Namespace):
    if args.logdir:
        writer = tensorboardX.SummaryWriter(log_dir=f'{args.logdir}/{args.name}')
    else:
        writer = None

    train_dataset = datasets.MNIST('mnist', True, transforms.ToTensor(), download=True)
    test_dataset = datasets.MNIST('mnist', False, transforms.ToTensor(), download=True)
    train_dataloader = data.DataLoader(train_dataset, args.batch_size, shuffle=True)
    valid_dataloader = data.DataLoader(test_dataset, args.batch_size, shuffle=True)

    model = FullyConnectedNetwork(28*28, args.hidden_sizes, 10)
    optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
    loss = nn.CrossEntropyLoss()

    if writer:
        writer.add_text('args', str(vars(args)))
        writer.add_graph(model, torch.ones([64, 1, 28, 28]))

    trainer = ignite.engine.create_supervised_trainer(model, optimizer, loss)
    metrics = {'accuracy': ignite.metrics.Accuracy()}
    evaluator = ignite.engine.create_supervised_evaluator(model, metrics)

    progress_bar = ignite.contrib.handlers.ProgressBar(persist=True)
    progress_bar.attach(trainer)
    early_stopping = ignite.handlers.EarlyStopping(2, lambda engine: engine.state.metrics['accuracy'], trainer)

    @trainer.on(ignite.engine.Events.EPOCH_COMPLETED)
    def log_results(engine):
        train_metrics = evaluator.run(train_dataloader).metrics
        valid_metrics = evaluator.run(valid_dataloader).metrics
        if writer:
            writer.add_scalar('accuracy/train', train_metrics['accuracy'], engine.state.epoch)
            writer.add_scalar('accuracy/valid', valid_metrics['accuracy'], engine.state.epoch)
            writer.add_scalar('error/train', 1 - train_metrics['accuracy'], engine.state.epoch)
            writer.add_scalar('error/valid', 1 - valid_metrics['accuracy'], engine.state.epoch)
        progress_bar.log_message(f"Accuracy = {train_metrics['accuracy']:.2%} / {valid_metrics['accuracy']:.2%}")
        progress_bar.n = progress_bar.last_print_n = 0
        early_stopping(evaluator)

    trainer.run(train_dataloader, args.max_epochs)

    if writer:
        writer.close()
    final_metrics = evaluator.run(valid_dataloader).metrics
    return final_metrics['accuracy']


def train_validation_error(*hidden_sizes, log_lr: float, batch_size: int, max_epochs: int, logdir: str):
    args = argparse.Namespace(name=petname.generate(), batch_size=batch_size, max_epochs=max_epochs,
                              hidden_sizes=[int(hs) for hs in hidden_sizes if hs != 0],
                              lr=10**log_lr, weight_decay=0,
                              logdir=logdir)
    val_accuracy = train(args)
    return 1 - val_accuracy


if __name__ == '__main__':
    cli = argparse.ArgumentParser()
    cli.add_argument('--name', type=str, default=petname.generate(), metavar='NAME',
                     help='experiment name (default: random-animal)')
    cli.add_argument('--hidden_sizes', type=int, default=[450], metavar='H', nargs='*',
                     help='list of hidden layer sizes (default: [450])')
    cli.add_argument('--max_epochs', type=int, default=25, metavar='E',
                     help='maximum number of epochs to train (default: 25)')
    cli.add_argument('--batch_size', type=int, default=32, metavar='B',
                     help='batch size for training and validation (default: 32)')
    cli.add_argument('--lr', type=float, default=0.0004, metavar='LR',
                     help='optimizer learning rate (default: 0.0004)')
    cli.add_argument('--weight_decay', type=float, default=0, metavar='W',
                     help='optimizer l2 weight decay (default: 0)')
    cli.add_argument('--logdir', type=str, default=None, metavar='DIR',
                     help='logging directory for TensorBoard (default: none)')
    args = cli.parse_args()

    train(args)

#! /usr/bin/env python3

import petname
import argparse

import ignite
import ignite.contrib.handlers
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data
import torchvision.datasets as datasets
import torchvision.transforms as transforms
import tensorboardX


class ConvolutionalNetwork(nn.Module):
    def __init__(self):
        super().__init__()
        self.features = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=5, stride=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.Conv2d(16, 32, kernel_size=5, stride=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2),
        )
        self.classifier = nn.Sequential(
            nn.Linear(4 * 4 * 32, out_features=512),
            nn.Linear(512, out_features=10),
        )

    def forward(self, input: torch.Tensor) -> torch.Tensor:
        features = self.features(input)
        flat = features.view([features.shape[0], -1])
        output = self.classifier(flat)
        return output


class ResidualNetwork(nn.Module):
    class Block(nn.Module):
        def __init__(self, in_channels: int, out_channels: int):
            super().__init__()
            self.conv1 = self.conv3x3(in_channels, out_channels, stride=2)
            self.bn1 = nn.BatchNorm2d(out_channels)
            self.relu = nn.ReLU(inplace=True)
            self.conv2 = self.conv3x3(out_channels, out_channels)
            self.bn2 = nn.BatchNorm2d(out_channels)

            self.convd = nn.Conv2d(in_channels, out_channels, kernel_size=1, stride=2, bias=False)
            self.bnd = nn.BatchNorm2d(out_channels)

        @staticmethod
        def conv3x3(in_channels: int, out_channels: int, stride: int = 1) -> nn.Conv2d:
            return nn.Conv2d(in_channels, out_channels, kernel_size=3, stride=stride, padding=1, bias=False)

        def forward(self, input: torch.Tensor) -> torch.Tensor:
            residual = self.conv1(input)
            residual = self.bn1(residual)
            residual = self.relu(residual)
            residual = self.conv2(residual)
            residual = self.bn2(residual)

            downsample = self.convd(input)
            downsample = self.bnd(downsample)

            out = residual + downsample
            out = self.relu(out)
            return out

    def __init__(self):
        super().__init__()
        self.conv1 = self.Block.conv3x3(1, 16)
        self.bn1 = nn.BatchNorm2d(16)
        self.relu = nn.ReLU(inplace=True)

        self.block1 = self.Block(16, 32)
        self.block2 = self.Block(32, 64)
        self.block3 = self.Block(64, 10)

        self.avgpool = nn.AvgPool2d(kernel_size=28)

    def forward(self, input: torch.Tensor) -> torch.Tensor:
        out = self.conv1(input)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.block1(out)
        out = self.block2(out)
        out = self.block3(out)

        out = self.avgpool(out)
        return out.squeeze()


network_types = {
    'convnet': ConvolutionalNetwork,
    'resnet': ResidualNetwork,
}


def train(args: argparse.Namespace):
    writer = tensorboardX.SummaryWriter(log_dir=f'{args.logdir}/{args.name}')

    train_dataset = datasets.MNIST('mnist', True, transforms.ToTensor(), download=True)
    test_dataset = datasets.MNIST('mnist', False, transforms.ToTensor(), download=True)
    train_dataloader = data.DataLoader(train_dataset, args.batch_size, shuffle=True)
    valid_dataloader = data.DataLoader(test_dataset, args.batch_size, shuffle=True)

    model = network_types[args.network_type]()
    optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
    loss = nn.CrossEntropyLoss()

    writer.add_text('args', str(vars(args)))
    writer.add_graph(model, torch.ones([64, 1, 28, 28]))

    trainer = ignite.engine.create_supervised_trainer(model, optimizer, loss)
    metrics = {'accuracy': ignite.metrics.Accuracy()}
    evaluator = ignite.engine.create_supervised_evaluator(model, metrics)

    progress_bar = ignite.contrib.handlers.ProgressBar(persist=True)
    progress_bar.attach(trainer)
    early_stopping = ignite.handlers.EarlyStopping(2, lambda engine: engine.state.metrics['accuracy'], trainer)

    @trainer.on(ignite.engine.Events.EPOCH_COMPLETED)
    def log_results(engine):
        train_metrics = evaluator.run(train_dataloader).metrics
        valid_metrics = evaluator.run(valid_dataloader).metrics
        writer.add_scalar('accuracy/train', train_metrics['accuracy'], engine.state.epoch)
        writer.add_scalar('accuracy/valid', valid_metrics['accuracy'], engine.state.epoch)
        writer.add_scalar('error/train', 1 - train_metrics['accuracy'], engine.state.epoch)
        writer.add_scalar('error/valid', 1 - valid_metrics['accuracy'], engine.state.epoch)
        progress_bar.log_message(f"Accuracy = {train_metrics['accuracy']:.2%} / {valid_metrics['accuracy']:.2%}")
        progress_bar.n = progress_bar.last_print_n = 0
        early_stopping(evaluator)

    trainer.run(train_dataloader, args.max_epochs)

    writer.close()
    final_metrics = evaluator.run(valid_dataloader).metrics
    return final_metrics['accuracy']


if __name__ == '__main__':
    cli = argparse.ArgumentParser()
    cli.add_argument('--name', type=str, default=petname.generate(), metavar='NAME',
                     help='experiment name (default: random-animal)')
    cli.add_argument('--network_type', type=str, choices=network_types, default='convnet', metavar='ARCH',
                     help='type of network architecture (default: convnet)')
    cli.add_argument('--max_epochs', type=int, default=25, metavar='E',
                     help='maximum number of epochs to train (default: 25)')
    cli.add_argument('--batch_size', type=int, default=64, metavar='B',
                     help='batch size for training and validation (default: 16)')
    cli.add_argument('--lr', type=float, default=0.0005, metavar='LR',
                     help='optimizer learning rate (default: 0.005)')
    cli.add_argument('--weight_decay', type=float, default=0, metavar='W',
                     help='optimizer l2 weight decay (default: 0)')
    cli.add_argument('--logdir', type=str, default='train', metavar='DIR',
                     help='logging directory for TensorBoard (default: train)')
    args = cli.parse_args()

    train(args)

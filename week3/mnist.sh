#! /usr/bin/env bash

python3 mnist.py --conv --logdir='conv'
python3 mnist.py --residual --logdir='residual'

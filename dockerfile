FROM pytorch/pytorch:latest

COPY . /workspace
WORKDIR /workspace
RUN pip install --requirement requirements.txt

EXPOSE 6006
EXPOSE 8888

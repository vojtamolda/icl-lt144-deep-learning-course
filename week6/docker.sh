#!/usr/bin/env bash

docker --volume D:/vojtech/Documents/Python/icl-lt144-deep-learning-course:/workspace \
       --publish=8888:8888 \
       jupyter notebook --allow-root --ip=0.0.0.0 --port=8888 &

docker --volume D:/vojtech/Documents/Python/icl-lt144-deep-learning-course:/workspace \
       --publish=6006:6006 \
       tensorboard --logdir=gan &
